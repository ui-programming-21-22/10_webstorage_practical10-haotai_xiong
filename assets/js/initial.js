var canvas = document.getElementById("myCanvas");
var context = canvas.getContext("2d");
var controls = document.getElementById("control");

let model = document.getElementById("model");
let form = document.forms["helloForm"];

let Game = document.getElementById("Game");
let UI = document.getElementById("UI");

console.log("initial.js detected");

//////////////////////////////////////////////////////////

if(typeof(Storage) !== "undefined") 
{
    // console.log("Local storage is supported.");
    // Local storage is available on your browser
    const username = localStorage.getItem('username');
    const score = localStorage.getItem('score');
    if (username)
    {
        let modelContent = model.children[0].children[2];
        model.style.display = "block";
        modelContent.innerHTML = "username: " + username + "<br>" + "score: " + score;
        let header = document.getElementById("main-header");
        header.innerHTML = "Hello " + username;
        let validateButton = document.getElementsByClassName("saved-data-accept")[0];
        let dismissButton = document.getElementsByClassName("saved-data-refusal")[0];
        validateButton.onclick = function(){
            Game.style.visibility = 'visible';
            Game.style.display = 'block';
            UI.style.visibility = 'hidden';
            Game.style.zIndex = '1';
            UI.style.zIndex = '-1';
            header.innerHTML = "Hello " + username;
            //console.log("resume fresh pressed");
        }
        dismissButton.onclick = function(){
            localStorage.clear();
            // css
            model.style.display = "none";
            form.style.display = "block";
            header.innerHTML = "Hello";
            // css setting
            Game.style.visibility = 'hidden';
            UI.style.visibility = 'visible';
            //console.log("start fresh pressed");
        }
    }
    else
    {
        console.log("no data in localStorage, loading new session")
        Game.style.visibility = 'visible';
        UI.style.visibility = 'visible';
    }
} 
else 
{
    console.log("Local storage is not supported.");
    // The condition isn't met, meaning local storage isn't supported
}

// Stores the item data
function validateForm()
{
    var x = document.forms["helloForm"]["name"].value;
    if (x == "") 
    {
        alert("I need to know your name so I can say Hello");
        // css
        model.style.display = "none";
        form.style.display = "block";
        header.innerHTML = "Hello";
        // css setting
        Game.style.visibility = 'hidden';
        UI.style.visibility = 'visible';
        return false;
    }
    else
    {
        alert("Hello there " + document.forms["helloForm"]["name"].value);
        //more advanced pt2: make a system that changes the webpage based on the inputted name 
        // css
        Game.style.visibility = 'visible';
        Game.style.display = 'block';
        UI.style.visibility = 'hidden';
        Game.style.zIndex = '1';
        UI.style.zIndex = '-1';
        header.innerHTML = "Hello " + x;
        console.log("Fine");
        event.preventDefault();
    }
    localStorage.setItem("username", x);
}